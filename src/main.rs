use std::os::unix::prelude::CommandExt;

use serde::Deserialize;

use crate::minecraft::ModSource;
use crate::modrinth::Modrinth;

mod modrinth;
mod minecraft;
mod utils;

fn get_source() -> Box<dyn ModSource> {
    return Box::new(Modrinth {});
}

fn main() {
    let source = get_source();

    let sodium = source.get_mod("sodium");

    match sodium {
        Ok(remote_mod) => {
            println!("{}", remote_mod.url())
        }
        Err(err) => { println!("Error : {}", err) }
    }
}