use serde::Deserialize;

use crate::minecraft::{ModSource, RemoteMod};
use crate::utils::request_json;

#[derive(Deserialize, Debug)]
struct Hashses {
    sha512: String,
    sha1: String,
}

impl Hashses {
    pub fn sha512(&self) -> &str {
        &self.sha512
    }
    pub fn sha1(&self) -> &str {
        &self.sha1
    }
}


#[derive(Deserialize, Debug)]
struct ModrinthDependency {
    version_id: String,
    project_id: String,
    dependency_type: String,
}

impl ModrinthDependency {
    pub fn version_id(&self) -> &str {
        &self.version_id
    }
    pub fn project_id(&self) -> &str {
        &self.project_id
    }
    pub fn dependency_type(&self) -> &str {
        &self.dependency_type
    }
}


#[derive(Deserialize, Debug)]
struct ModrinthFile {
    hashes: Hashses,
    url: String,
    filename: String,
}

impl ModrinthFile {
    pub fn hashes(&self) -> &Hashses {
        &self.hashes
    }
    pub fn url(&self) -> &str {
        &self.url
    }
    pub fn filename(&self) -> &str {
        &self.filename
    }
}

#[derive(Deserialize, Debug)]
pub struct ModrinthVersion {
    id: String,
    version_number: String,
    files: Vec<ModrinthFile>,
    //dependencies: Vec<ModrinthDependency>, TODO : To fix - Empty arrays cause error
}

impl ModrinthVersion {
    fn id(&self) -> &String {
        &self.id
    }

    fn download_url(&self) -> String {
        let url = String::from("Caca");

        return url;
    }

    fn dependencies(&self) -> Result<Vec<ModrinthVersion>, String> {
        /*let files = self.dependencies.iter().map(|f| String::from(&f.version_id)).collect::<Vec<String>>().join(", ");

        let list = request_json::<Vec<ModrinthVersion>>(format!("https://api.modrinth.com/v2/versions/?ids=[{files}]"));

        return match list {
            Ok(result) => {
                return Ok(result);
            }
            Err(err) => { Err(err) }
        };*/
        Ok(vec![])
    }

    fn convert(&self) -> Result<RemoteMod, String> {
        let file = self.main_file();

        return match file {
            None => {
                Err(String::from("Unable to find an URL"))
            }
            Some(file) => {
                let deps_result = self.dependencies();

                return match deps_result {
                    Ok(deps) => {
                        let converted_deps: Result<Vec<RemoteMod>, String> = deps.into_iter().map(|dep| dep.convert()).collect();

                        return match converted_deps {
                            Ok(dep) => {
                                return Ok(RemoteMod::new(file.filename.clone(), file.url.clone(), file.hashes.sha512.clone(), dep));
                            }
                            Err(err) => { Err(err) }
                        };
                    }
                    Err(err) => { Err(err) }
                };
            }
        };
    }

    fn main_file(&self) -> Option<&ModrinthFile> {
        return self.files.get(0);
    }
}

pub struct Modrinth {}

impl ModSource for Modrinth {
    fn get_mod(&self, slug: &str) -> Result<RemoteMod, String> {
        let result = request_json::<Vec<ModrinthVersion>>(format!("https://api.modrinth.com/v2/project/{slug}/version"));

        return match result {
            Ok(data) => {
                let option = data.get(0);

                return match option {
                    None => { Err(String::from("Unable to find a version")) }
                    Some(mod_version) => {
                        let result = mod_version.convert();

                        return match result {
                            Ok(remote_mod) => { Ok(remote_mod) }
                            Err(err) => { Err(err) }
                        }
                    }
                };
            }
            Err(err) => { Err(err) }
        };
    }
}