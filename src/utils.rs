use serde::de::DeserializeOwned;

pub fn request_json<T: DeserializeOwned>(url: String) -> Result<T, String> {
    let result = reqwest::blocking::get(url);

    return match result {
        Ok(res) => {
            return if res.status().is_success() {
                let js = res.json::<T>();
                match js {
                    Ok(mod_obj) => {
                        Ok(mod_obj)
                    }
                    Err(err) => {
                        Err(err.to_string())
                    }
                }
            } else {
                let text = res.text();
                match text {
                    Ok(text) => {
                        Err(text)
                    }
                    Err(err) => {
                        Err(err.to_string())
                    }
                }
            };
        }
        Err(err) => {
            Err(err.to_string())
        }
    };
}