use serde::Deserialize;

#[derive(Deserialize)]
pub enum Side {
    CLIENT,
    SERVER,
}

#[derive(Deserialize)]
pub struct LocalMod {
    slug: String,
    source: String,
    specific_version: Option<String>,
    beta: Option<bool>,
}

impl LocalMod {
    pub fn new(slug: String, source: String, specific_version: Option<String>, beta: Option<bool>) -> Self {
        Self { slug, source, specific_version, beta }
    }

    pub fn slug(&self) -> &str {
        &self.slug
    }
    pub fn source(&self) -> &str {
        &self.source
    }
    pub fn specific_version(&self) -> &Option<String> {
        &self.specific_version
    }
    pub fn beta(&self) -> Option<bool> {
        self.beta
    }
}

#[derive(Deserialize)]
pub struct PackConfig {
    version: String,
    side: Side,
    loader: String,
}

impl PackConfig {
    pub fn new(version: String, side: Side, loader: String) -> Self {
        Self { version, side, loader }
    }

    pub fn version(&self) -> &str {
        &self.version
    }
    pub fn side(&self) -> &Side {
        &self.side
    }

    pub fn loader(&self) -> &str {
        &self.loader
    }
}

#[derive(Deserialize)]
pub struct PackManifest {
    config: PackConfig,
    mods: Box<[LocalMod]>,
}

impl PackManifest {
    pub fn new(config: PackConfig, mods: Box<[LocalMod]>) -> Self {
        Self { config, mods }
    }

    pub fn config(&self) -> &PackConfig {
        &self.config
    }
    pub fn mods(&self) -> &Box<[LocalMod]> {
        &self.mods
    }
}

pub struct RemoteMod {
    filename: String,
    url: String,
    hash: String,
    dependencies: Vec<Self>,
}

impl RemoteMod {
    pub fn new(filename: String, url: String, hash: String, dependencies: Vec<Self>) -> Self {
        Self { filename, url, hash, dependencies }
    }
    pub fn filename(&self) -> &str {
        &self.filename
    }
    pub fn url(&self) -> &str {
        &self.url
    }
    pub fn hash(&self) -> &str {
        &self.hash
    }
    pub fn dependencies(&self) -> &Vec<Self> {
        &self.dependencies
    }
}

pub trait ModSource {
    fn get_mod(&self, id: &str) -> Result<RemoteMod, String>;
}
